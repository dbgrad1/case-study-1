# from flask import request
# from flask import abort
# import requests
# import json

# dao = 'http://localhost:5000'

# response = requests.get(dao + '/getDealData')
# if response.status_code != 200 : abort(401)

# data = json.loads(response.content)

import pandas as pd


##tentative profit and loss functions
##not attached to a class or data yet

##quantity: quantity of current deal
##price: price of current deal
##prevprice: price of last deal with opposite type
    ##if no previous deal, then base price
def calcRealisedProfit(quantity,price,prevprice):
    return (quantity * (price - prevprice))

##realProfit: aggregate realised profit of trades
##prevprice: price of last deal with opposite type
##quantity: quantity of current deal
##prevquant: quantity of last deal with opposite type
##currprice: price of deal at end of period
def calcEffectiveProfit(realProfit,quantity,prevprice,prevquant,currprice):
    effprofit = realProfit + ((prevquant - quantity) * (currprice - prevprice))
    return effprofit

##test data
tdata = [{"instrumentName": "Galactia", "cpty": "Lewis", "price": 9964.235074757127, "type": "S", "quantity": 71, "time": "11-Aug-2019 (12:07:06.471252)"},
        {"instrumentName": "Deuteronic", "cpty": "Lewis", "price": 8540.619061084186, "type": "B", "quantity": 2, "time": "11-Aug-2019 (12:07:06.669213)"},
        {"instrumentName": "Lunatic", "cpty": "John", "price": 1830.8271150278415, "type": "B", "quantity": 1, "time": "11-Aug-2019 (12:07:06.944790)"},
        {"instrumentName": "Astronomica", "cpty": "Nidia", "price": 3409.1510219490383, "type": "S", "quantity": 3, "time": "11-Aug-2019 (12:07:07.042573)"},
        {"instrumentName": "Heliosphere", "cpty": "Lina", "price": 7748.8136364925085, "type": "S", "quantity": 430, "time": "11-Aug-2019 (12:07:07.106903)"},
         {"instrumentName": "Galactia", "cpty": "Lewis", "price": 10051.25633626954, "type": "B", "quantity": 11, "time": "11-Aug-2019 (12:07:07.876999)"},
         {"instrumentName": "Heliosphere", "cpty": "Lina", "price": 7826.37710272039, "type": "B", "quantity": 19, "time": "11-Aug-2019 (12:07:08.054357)"}
         ]

##list of unique trader names from data
dealers = []
instruments = []



def profitloss(data):


    for row in data:
        if row["cpty"] not in dealers:
            dealers.append(row['cpty'])

    ##list of unique instrument names from data
    for row in data:
        if row["instrumentName"] not in instruments:
            instruments.append(row["instrumentName"])

    realProfitByInst = 0 #realised profit for each instrument
    effProfitByInst = 0 #effective profit for each instrument
    realisedProfit = 0 #total realised profit for each dealer
    effectiveProfit = 0 #total effective profit for each dealer
    realProfitByDealer = {} #realised profit divided by dealer
    effectProfitByDealer = {} #effective profit divided by dealer

    #iterates through dealers and instruments to calculate profit per instrument per dealer
    for dealer in dealers:
        prices = {}

        for instrument in instruments:
            instrument_price_dealer = []
            for row in data:

                if(row["instrumentName"] == instrument and row["cpty"] == dealer):
                    instrument_price_dealer.append(row["price"])
                    #print(instrument_price_dealer, instrument, prices)
                    prices.update({instrument:instrument_price_dealer})

                    if len(prices[instrument]) > 1:
                        realProfitByInst += calcRealisedProfit(row["quantity"],prices[instrument][-1],prices[instrument][-2])
                        effProfitByInst += calcEffectiveProfit(realProfitByInst,row["quantity"],row["price"],row["quantity"],row["price"])
                    print(dealer, instrument, prices)
                realisedProfit += realProfitByInst
                effectiveProfit += effProfitByInst
                realProfitByInst = 0
                effProfitByInst = 0
        realProfitByDealer.update({dealer : realisedProfit})
        effectProfitByDealer.update({dealer : effectiveProfit})
        realisedProfit = 0
        effectiveProfit = 0

    data_table = pd.DataFrame(data)
    data_table['avgPrice'] = data_table.price * data_table.quantity
    # print(data_table)
    averages = data_table[['instrumentName','type','avgPrice']].groupby(by=['instrumentName','type']).mean().reset_index()
    print(averages.to_json(orient='records'))

    data_table = pd.DataFrame(data)
    data_table['time'] = pd.to_datetime(data_table['time'], format = '%d-%b-%Y (%H:%M:%S.%f)')
    data_table = data_table.sort_values(by='time')
    print(data_table)

    print("Realised Profit By Dealer:", realProfitByDealer)
    print("Effective Profit By Dealer:", effectProfitByDealer)
    #print(prices)


profitloss(tdata)



