import pandas as pd

def calcProfits(data):
    orgData = pd.DataFrame(data)

    #create instrument/currPrice table (ICP)
    currPriceTable = orgData
    currPriceTable['time'] = pd.to_datetime(currPriceTable['time'])
    currPriceTable = currPriceTable.sort_values(by='time')
    currPriceTable = currPriceTable.drop_duplicates(subset=['instrument'], keep='last')[['instrument','price']]
    currPriceTable = currPriceTable.reset_index(drop=True)

    #create table of instrument/dealer combinations
    instDealTable = pd.DataFrame(data)
    instDealTable = instDealTable[['instrument','dealer']]
    instDealTable = instDealTable.drop_duplicates(keep='first')
    instDealTable = instDealTable.reset_index(drop=True)
    instDealTable['realizedProfit'] = 0
    instDealTable['effectiveProfit'] = 0

    #create instrument/dealer/etc table (IDE)
    for i in range(len(instDealTable)):
        dealTable = pd.DataFrame(data)
        dealTable = dealTable[(dealTable.instrument == instDealTable.loc[i,'instrument']) & (dealTable.dealer == instDealTable.loc[i,'dealer'])]
        dealTable['time'] = pd.to_datetime(dealTable['time'])
        dealTable = dealTable.sort_values(by='time')
        dealTable['prevPrice'] = dealTable['price'].shift(periods = 1)
        dealTable['prevQuantity'] = dealTable['quantity'].shift(periods = 1)
        dealTable['prevType'] = dealTable['type'].shift(periods = 1)
        dealTable = dealTable.drop(['idDeal','time'], axis = 1)
        dealTable = dealTable.reset_index(drop=True)
        dealTable = dealTable[1:]
        dealTable = dealTable.reset_index(drop=True)
        for i in range(len(dealTable)-1):
            if dealTable.loc[i,'type'] == dealTable.loc[i,'prevType']: 
                dealTable.loc[i+1,'prevQuantity'] =  dealTable.loc[i,'quantity'] + dealTable.loc[i,'prevQuantity']
        dealTable = dealTable[dealTable.type != dealTable.prevType]
        dealTable = dealTable[dealTable.type == 'S']
        dealTable['realizedProfit'] = dealTable.quantity * (dealTable.price - dealTable.prevPrice)
        instDealTable.loc[i,'realizedProfit'] = dealTable['realizedProfit'].sum()
        dealTable['currentPrice'] = currPriceTable[currPriceTable.instrument == instDealTable.loc[i,'instrument']].iloc[0,1]
        dealTable['effectiveProfit'] = dealTable.realizedProfit + (dealTable.prevQuantity - dealTable.quantity) * (dealTable.currentPrice - dealTable.prevPrice)
        instDealTable.loc[i,'effectiveProfit'] = dealTable['effectiveProfit'].sum()
        #print(dealTable)
        #print(instDealTable)
    profitTable = instDealTable.groupby(by='dealer').sum().reset_index()
    return profitTable.to_json(orient='records'), 200

def calcAveragePrice(data):
    
    data_table = pd.DataFrame(data)
    data_table['avgPrice'] = data_table.price
    averages = data_table[['instrument','type','avgPrice']].groupby(by=['instrument','type']).mean().reset_index()
    # print(averages)
    return averages.to_json(orient='records'), 200



