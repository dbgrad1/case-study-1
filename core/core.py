from flask import Flask, Response, jsonify
from flask_cors import CORS
from mysql.connector import Error
import requests
import json
import pandas as pd
from flask import request
from flask import abort
import profitloss as profitloss

app = Flask(__name__)
CORS(app)

dao = 'http://dao2-crocagile.apps.dbgrads-6eec.openshiftworkshop.com'
generator = 'http://datagen-crocagile.apps.dbgrads-6eec.openshiftworkshop.com'
# dao = 'http://localhost:8080'
# generator = 'http://localhost:8082'

@app.route('/')
def stream():
    messages = requests.get(generator + '/stream', stream=True)
    def eventStream():
        for message in messages.iter_lines(chunk_size=1):
            sendToDAO(message.decode('utf-8'))
            print(message.decode('utf-8'))
            yield message
    
    return Response(eventStream(), mimetype='text/event-stream')

def sendToDAO(message):
    requests.post(dao + '/insert', json.dumps(message), headers = {'content-type':'application/json'})


@app.route('/getHistory')
def getHistory():
    try:
        response = requests.get(dao + '/getDealData')
        if response.status_code != 200 : abort(401)
        if len(request.data) <= 2: abort(404)
        jsonData = request.get_json()
        # jsonData = json.loads(req)

        if ('from' not in jsonData.keys()) & ('to' not in jsonData.keys()):
            abort(404)
        timeStampFrom = jsonData['from']
        timeStampTo = jsonData['to']
        instrument = None
        if 'instrument' in jsonData.keys():
            instrument = jsonData['instrument']
        type = None
        if 'type' in jsonData.keys():
            type = jsonData['type']
        dealer = None
        if 'dealer' in jsonData.keys():
            dealer = jsonData['dealer']
            
        history = pd.DataFrame(json.loads(response.content))
        history = history.drop(['idDeal'], axis = 1)
        # select distinct rows
        history = history.drop_duplicates(keep='first')

        

        # data formatting???
        history['time'] = pd.to_datetime(history['time'])
        history['time'] = history['time'].map(lambda x: x.astype('int64')/1e9)
        # filter rows
        history = history[(history.time >= timeStampFrom) & (history.time <= timeStampTo)]
        
        if instrument is not None:
            history = history[history.instrument == instrument]
        if type is not None:
            history = history[history.type == type]
        if dealer is not None:
            history = history[history.dealer == dealer]

        result = history.to_json(orient='records', date_format = 'iso')
        
        return result

    except Error as e:
        print(e.msg)
        return jsonify({'error': e.msg}), 401


@app.route('/calcProfits')
def calcProfits():
    
    response = requests.get(dao + '/getDealData')
    if response.status_code != 200 : abort(401)

    data = json.loads(response.content)
    return profitloss.calcProfits(data)

@app.route('/getAvgPrices')
def getAvgPrices():

    response = requests.get(dao + '/getDealData')
    if response.status_code != 200 : abort(401)
    if len(request.data) <= 2: abort(404)
    jsonData = request.get_json()

    if ('from' not in jsonData.keys()) & ('to' not in jsonData.keys()):
        abort(404)
    dateFrom = jsonData['from']
    dateTo = jsonData['to']

    data = pd.DataFrame(json.loads(response.content))
    data = data.drop(['idDeal'], axis = 1)
    data = data.drop_duplicates(keep='first')
    data['time'] = pd.to_datetime(data['time'])
    dateTo = pd.to_datetime(dateTo)
    dateFrom = pd.to_datetime(dateFrom)

    data['time'] = data['time'].dt.tz_localize(None)
    data = data[(data.time >= dateFrom.tz_localize(None)) & (data.time <= dateTo.tz_localize(None))]
    
    return profitloss.calcAveragePrice(data)

# guess it will never start working 
@app.route('/getEndingPositions')
def getEndingPositions():

    response = requests.get(dao + '/getDealData')
    if response.status_code != 200 : abort(401)

    data = pd.DataFrame(json.loads(response.content))  

    # need to get the date and to get all positions from this day 

    return 'no'    

        
def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))

if __name__ == '__main__':
    bootapp()