# from profitloss import calcEffectiveProfit, calcRealisedProfit
from profitloss import calcProfits, calcAveragePrice

# realised profit test from the wiki example
def test_CalcRealisedProfit():
    # assert calcRealisedProfit(50, 11, 10) == 50
    assert calcRealisedProfit(25, 10, 15) == -125

# using the realised profit to calculate the effective profit
def test_calcEffectiveProfit():
    # assert calcEffectiveProfit(50,50,10,100,13) == 200
    assert calcEffectiveProfit(-125, 25, 15, 75, 12) == -275

##test data
test_data = [{"idDeal": 1, "instrument": "Galactia", "dealer": "Lewis", "price": 9964.235074757127, "type": "S", "quantity": 71, "time": "11 Aug 2019 12:07:06.471252"},
        {"idDeal": 2, "instrument": "Deuteronic", "dealer": "Lewis", "price": 8540.619061084186, "type": "B", "quantity": 2, "time": "11 Aug 2019 12:07:06.669213"},
        {"idDeal": 3, "instrument": "Lunatic", "dealer": "John", "price": 1830.8271150278415, "type": "B", "quantity": 1, "time": "11 Aug 2019 12:07:06.944790"},
        {"idDeal": 4, "instrument": "Astronomica", "dealer": "Nidia", "price": 3409.1510219490383, "type": "S", "quantity": 3, "time": "11 Aug 2019 12:07:07.042573"},
        {"idDeal": 5, "instrument": "Heliosphere", "dealer": "Lina", "price": 7748.8136364925085, "type": "S", "quantity": 430, "time": "11-Aug-2019 12:07:07.106903"},
         {"idDeal": 6, "instrument": "Galactia", "dealer": "Lewis", "price": 10051.25633626954, "type": "B", "quantity": 11, "time": "11 Aug 2019 12:07:07.876999"},
         {"idDeal": 7, "instrument": "Heliosphere", "dealer": "Lina", "price": 7826.37710272039, "type": "B", "quantity": 19, "time": "11 Aug 2019 12:07:08.054357"}
         ]



test_data = [{"idDeal": 1, "instrument": "Galactia", "dealer": "Lewis", "price": 9964.235074757127, "type": "S", "quantity": 71, "time": "11 Aug 2019 12:07:06.471252"},
        {"idDeal": 2, "instrument": "Deuteronic", "dealer": "Lewis", "price": 8540.619061084186, "type": "B", "quantity": 2, "time": "11 Aug 2019 12:07:06.669213"},
        {"idDeal": 8, "instrument": "Deuteronic", "dealer": "Lewis", "price": 8742.619061084186, "type": "S", "quantity": 3, "time": "11 Aug 2019 12:07:06.679213"},
        {"idDeal": 3, "instrument": "Lunatic", "dealer": "John", "price": 1830.8271150278415, "type": "B", "quantity": 1, "time": "11 Aug 2019 12:07:06.944790"},
        {"idDeal": 4, "instrument": "Astronomica", "dealer": "Nidia", "price": 3409.1510219490383, "type": "S", "quantity": 3, "time": "11 Aug 2019 12:07:07.042573"},
        {"idDeal": 5, "instrument": "Heliosphere", "dealer": "Lina", "price": 7748.8136364925085, "type": "S", "quantity": 430, "time": "11-Aug-2019 12:07:07.106903"},
        {"idDeal": 6, "instrument": "Galactia", "dealer": "Lewis", "price": 10051.25633626954, "type": "B", "quantity": 11, "time": "11 Aug 2019 12:07:07.876999"},
        {"idDeal": 1, "instrument": "Galactia", "dealer": "Lewis", "price": 9700.235074757127, "type": "S", "quantity": 71, "time": "11 Aug 2019 12:07:09.471252"},
        {"idDeal": 7, "instrument": "Galactia", "dealer": "Lina", "price": 5826.37710272039, "type": "B", "quantity": 19, "time": "11 Aug 2019 12:08:08.054357"},
             ]

a = calcProfits(test_data)
print("FIRST TEST: ")
print(a)
# print(type(a[0]))

b = calcAveragePrice(test_data)
print(b)


test_data2 = [{"idDeal": 1, "instrument": "Galactica", "dealer": "Lewis", "price": 10, "type": "B", "quantity": 100,
              "time": "11 Aug 2019 12:07:06.471252"},
             {"idDeal": 2, "instrument": "Galactica", "dealer": "Lewis", "price": 11, "type": "S", "quantity": 50,
              "time": "11 Aug 2019 12:07:06.669213"},
             {"idDeal": 3, "instrument": "Galactica", "dealer": "John", "price": 13, "type": "B", "quantity": 1,
              "time": "11 Aug 2019 12:07:06.944790"}]


print("SECOND TEST: ")
print("", calcProfits(test_data2))
print("", calcAveragePrice(test_data2))
