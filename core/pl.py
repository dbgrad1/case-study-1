import pandas as pd

def calcRealizedProfit(data):

    ##tentative profit and loss functions

    ##quantity: quantity of current deal
    ##price: price of current deal
    ##prevprice: price of last deal with opposite type
        ##if no previous deal, then base price
    def calcRealisedProfit(quantity,price,prevprice):
        return (quantity * (price - prevprice))

    ##realProfit: aggregate realised profit of trades
    ##prevprice: price of last deal with opposite type
    ##quantity: quantity of current deal
    ##prevquant: quantity of last deal with opposite type
    ##currprice: price of deal at end of period
    def calcEffectiveProfit(realProfit,quantity,prevprice,prevquant,currprice):
        effprofit = realProfit + ((prevquant - quantity) * (currprice - prevprice))
        return effprofit

    ##list of unique trader names from data
    dealers = []
    instruments = []

    for row in data:
        if row["cpty"] not in dealers:
            dealers.append(row['cpty'])

    ##list of unique instrument names from data
    for row in data:
        if row["instrumentName"] not in instruments:
            instruments.append(row["instrumentName"])

    realProfitByInst = 0 #realised profit for each instrument
    effProfitByInst = 0 #effective profit for each instrument
    realisedProfit = 0 #total realised profit for each dealer
    effectiveProfit = 0 #total effective profit for each dealer
    realProfitByDealer = {} #realised profit divided by dealer
    effectProfitByDealer = {} #effective profit divided by dealer

    #iterates through dealers and instruments to calculate profit per instrument per dealer
    for dealer in dealers:
        for instrument in instruments:
            for row in data:
                if(row["instrumentName"] == dealer and row["cpty"] == instrument):
                    realProfitByInst += calcRealisedProfit(row["quantity"],row["price"],row["price"])
                    effProfitByInst += calcEffectiveProfit(realProfitByInst,row["quantity"],row["price"],row["quantity"],row["price"])
                realisedProfit += realProfitByInst
                effectiveProfit += effProfitByInst
                realProfitByInst = 0
                effProfitByInst = 0
        realProfitByDealer.update({dealer : realisedProfit})
        effectProfitByDealer.update({dealer : effectiveProfit})
        realisedProfit = 0
        effectiveProfit = 0

    data_table = pd.DataFrame(data)
    data_table['time'] = pd.to_datetime(data_table['time'], format = '%d-%b-%Y (%H:%M:%S.%f)')
    data_table = data_table.sort_values(by='time')
    print(data_table)

def calcAveragePrice(data):

    data_table = pd.DataFrame(data)
    data_table['avgPrice'] = data_table.price * data_table.quantity
    averages = data_table[['instrument','type','avgPrice']].groupby(by=['instrument','type']).mean().reset_index()
    return averages.to_json(orient='records'), 200




<<<<<<< HEAD

=======
# print("Realised Profit By Dealer:",realProfitByDealer)
# print("Effective Profit By Dealer:",effectProfitByDealer)



##test data
# data = [{"instrumentName": "Galactia", "cpty": "Lewis", "price": 9964.235074757127, "type": "S", "quantity": 71, "time": "11-Aug-2019 (12:07:06.471252)"},
#         {"instrumentName": "Deuteronic", "cpty": "Lewis", "price": 8540.619061084186, "type": "B", "quantity": 2, "time": "11-Aug-2019 (12:07:06.669213)"},
#         {"instrumentName": "Lunatic", "cpty": "John", "price": 1830.8271150278415, "type": "B", "quantity": 1, "time": "11-Aug-2019 (12:07:06.944790)"},
#         {"instrumentName": "Astronomica", "cpty": "Nidia", "price": 3409.1510219490383, "type": "S", "quantity": 3, "time": "11-Aug-2019 (12:07:07.042573)"},
#         {"instrumentName": "Heliosphere", "cpty": "Lina", "price": 7748.8136364925085, "type": "S", "quantity": 430, "time": "11-Aug-2019 (12:07:07.106903)"}
#         ]
>>>>>>> dev
