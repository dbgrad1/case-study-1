import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './css/style.css';

const API_url='http://localhost:5000'

function SecretView() {
    const [email, setEmail] = useState("");
    const [secretStaff, setSecretStaff] = useState("");
    
  
    useEffect(() => {
      const request = new Request(API_url+'/secretStaff', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': localStorage.getItem('access_token')
        }
      })
      fetch(request)
        .then(res => res.json())
        .then(
          (result) => {
            console.log(result.email);
            console.log(result.secretStaff);
            setEmail(result.email);
            setSecretStaff(result.secretStaff);
          });
    });
  
    if ((localStorage.getItem('access_token') == null))
        return <Redirect to='/login'/>;
    else 
      return (
        <div className='wrapper flex-center flex-column'>
          Hi, {email}!
          <br />
          secretStaff is "{secretStaff}"
        </div>
      );
  }

export default SecretView