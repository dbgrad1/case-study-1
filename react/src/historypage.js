import React, { useState, useEffect } from "react";
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './css/style.css';

//import {sideBar, topBar, viewFooter} from './fixedElements.js';

const API_url = 'http://localhost:5001'

function HistoryView() {

    return (
        <div>
            <div className="sidenav">
                <h3><b>Crocagile</b></h3>
                <a href="/graph">Live Data</a>
                <a href="/history">History</a>
                <a href="/position">Position</a>
            </div>
        
            <nav className="navbar navbar-dark bg-dark">
                <a className="navbar-brand" href="#">Navbar</a>
            </nav>

            <div className="heading">
                <h1 align = "center"><b>History</b></h1>
            </div>

            <div className="footer">
                <p>© 2019 Copyright: Crocagile Ltd.</p>
            </div>*/
            <HistoryMain />
        </div>        
    );
}

function HistoryMain() {
    const [deals, setDeals] = useState([]);
    const [from, setFrom] = useState(undefined);
    const [to, setTo] = useState(undefined);
    const [instrument, setInstrument] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();
        let f = new Date(from).getTime()/1000;
        let t = new Date(to).getTime()/1000;
        console.log([f, t, instrument])
        const request = new Request(API_url+'/getHistory', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({from:f,to:t,instrument:instrument})
        });
        fetch(request)
        .then(res => {
            if (res.status === 200) {
                res.json()
                .then(res => {
                    console.log(res);
                    setDeals(res);
                });
            }
        });
    }

    const renderTableHeader = () => {
        if (deals === undefined || deals.length == 0) {
            return (<div></div>)
        } else {
            console.log(deals)
            let header = Object.keys(deals[0])
            return header.map((key, index) => {
                return <th key={index}>{key.toUpperCase()}</th>
            })
        }
    }

    const renderTableData = () => {
        if (deals === undefined || deals.length == 0) {
            return (<div></div>)
        } else {
            return deals.map((deal, index) => {
                const { dealer, instrument, price, quantity, time, type } = deal //destructuring
                return (
                    <tr key={dealer}>
                        <td>{dealer}</td>
                        <td>{instrument}</td>
                        <td>{price}</td>
                        <td>{quantity}</td>
                        <td>{time}</td>
                        <td>{type}</td>
                    </tr>
                )
            })
        }
    }

    return (            
        
        <div>
            <div className='wrapper flex-center flex-column'>
                <form>
                    <div className="form-group">
                        <div className = "row">
                            <div className = "col-md-4">
                            <label>from:</label>
                            <input type="datetime-local" id="from" className="form-control" value={from} onChange={e => setFrom(e.target.value)} />  
                            </div>

                            <div className = "col-md-4">
                            <label>to:</label>
                            <input type="datetime-local" id="to" className="form-control" value={to} onChange={e => setTo(e.target.value)}/>
                            </div>

                            <div className = "col-md-3">
                            <label>instrument:</label>
                            <input type="text" id="instrument" className="form-control" value={instrument} onChange={e => setInstrument(e.target.value)}/>
                            </div>

                            <div className = "col-md-1" id="submitbtn">
                            <button type="submit" className="btn btn-default" onClick = {handleSubmit}> Search </button>  
                            </div>

                        </div>
                                    
                    </div>
                </form>
            </div>

            <table id='instruments'>
                <tbody>
                <tr>{renderTableHeader()}</tr>
                    {renderTableData()}
                </tbody>
            </table>
        </div>
    )
}

export default HistoryMain;