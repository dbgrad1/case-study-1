import React, { useState } from "react";
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './css/style.css';

export class sideBar extends React.Component {
  render(){
    return (
      <div classNameName="sidenav">
      <h3><b>Crocagile</b></h3>
      <a href="#">Graphs</a>
      <a href="#">History</a>
      <a href="#">Position</a>
    </div>
    );
  }
}

export class topBar extends React.Component {
  render(){
    return (
      <nav className="navbar navbar-dark bg-dark">
        <a className="navbar-brand" href="#">Navbar</a>
      </nav>
    );
  }
}

export class viewFooter extends React.Component {
  render(){
    return (
      <div className="footer">
        <p>© 2019 Copyright: Crocagile Ltd.</p>
      </div>
    );
  }
}

//replace with parameter
export class pageTitle extends React.Component {
  render(){
    return (
      <div className="row">
      <p> </p>
      <h1><b>History</b></h1>
    </div>
    );
  }
}

//export {sideBar, topBar, viewFooter, pageTitle}




