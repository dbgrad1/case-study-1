import React, { useState } from "react";
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './css/style.css';
import { Line } from 'react-chartjs-2';
import trimHistory from './trim_history.js';
import graphMaker from './graphmaker.js';

var myDeals = [{dealer:'Lewis', instrument:'Galactia', price:'4848', quantity:'96', time:'', type:'Sell'},
    {dealer:'Selvyn', instrument:'Eclipse', price:'9965', quantity:'79', time:'', type:'Sell'},
    {dealer:'Victoria', instrument:'Galactia', price:'7548', quantity:'98', time:'', type:'Buy'},
    {dealer:'Lewis', instrument:'Eclipse', price:'6358', quantity:'56', time:'', type:'Buy'},
    {dealer:'Ed', instrument:'Galactia', price:'4898', quantity:'95', time:'', type:'Sell'},
    {dealer:'Scott', instrument:'Eclipse', price:'7321', quantity:'66', time:'', type:'Buy'},
    {dealer:'Victoria', instrument:'Galactia', price:'7548', quantity:'98', time:'', type:'Buy'},
    {dealer:'Lewis', instrument:'Nebula', price:'6358', quantity:'56', time:'', type:'Buy'},
    {dealer:'Ed', instrument:'Nebula', price:'4898', quantity:'95', time:'', type:'Sell'},
    {dealer:'Scott', instrument:'Nebula', price:'7321', quantity:'66', time:'', type:'Buy'},
    {dealer:'Selvyn', instrument:'Eclipse', price:'9965', quantity:'79', time:'', type:'Sell'},
    {dealer:'Selvyn', instrument:'Eclipse', price:'9965', quantity:'79', time:'', type:'Sell'},
    {dealer:'Selvyn', instrument:'Eclipse', price:'9965', quantity:'79', time:'', type:'Sell'},
    {dealer:'Selvyn', instrument:'Eclipse', price:'9965', quantity:'79', time:'', type:'Sell'},
    {dealer:'Lewis', instrument:'Nebula', price:'6358', quantity:'56', time:'', type:'Buy'},
    {dealer:'Ed', instrument:'Nebula', price:'4898', quantity:'95', time:'', type:'Sell'},
    {dealer:'Lewis', instrument:'Nebula', price:'6358', quantity:'56', time:'', type:'Buy'},
    {dealer:'Ed', instrument:'Nebula', price:'4898', quantity:'95', time:'', type:'Sell'},
    {dealer:'Lewis', instrument:'Nebula', price:'6358', quantity:'56', time:'', type:'Buy'},
    {dealer:'Ed', instrument:'Nebula', price:'4898', quantity:'95', time:'', type:'Sell'},
    {dealer:'Ed', instrument:'Galactia', price:'4898', quantity:'95', time:'', type:'Sell'},
    {dealer:'Ed', instrument:'Galactia', price:'4898', quantity:'95', time:'', type:'Sell'},
    {dealer:'Ed', instrument:'Galactia', price:'4898', quantity:'95', time:'', type:'Sell'},
    {dealer:'Ed', instrument:'Galactia', price:'4898', quantity:'95', time:'', type:'Sell'},
    {dealer:'Ed', instrument:'Galactia', price:'4898', quantity:'95', time:'', type:'Sell'}];

var myInstruments = trimHistory(myDeals,20);

const API_url = 'http://localhost:6001'

var data = graphMaker(myInstruments,20);

function GraphApp() {

    return (
        <React.Fragment>

            <div className="sidenav">
                <h3><b>Crocagile</b></h3>
                <a href="/graph">Live Data</a>
                <a href="/history">History</a>
                <a href="/position">Position</a>
            </div>

            <nav className="navbar navbar-dark bg-dark">
                <a className="navbar-brand" href="#">Navbar</a>
            </nav>

            <div className="heading">
                <h1 align="center"><b>Live Data: Last 20 deals</b></h1>
            </div>

            <div className = "row">
                <div className = "col-md-9">
                <div className = "myGraph">
                    <Line data={data} />
                </div>
                </div>

                <div className = "col-md-3">
                <div className = "avgTable">
                    <AvgPriceTable />
                </div>
                </div>

            </div>

            <div className="footer">
                <p>© 2019 Copyright: Crocagile Ltd.</p>
            </div>

        </React.Fragment>
    );
}

function ShowGraph() {

    return (
        <React.Fragment>

            <div className="heading">
                <h1 align="center"><b>Live Data: Last 20 deals</b></h1>
            </div>

            <div className = "row">
                <div className = "col-md-9">
                <div className = "myGraph">
                    <Line data={data} />
                </div>
                </div>

                <div className = "col-md-3">
                <div className = "avgTable">
                    <AvgPriceTable />
                </div>
                </div>

            </div>


        </React.Fragment>
    );
}


class AvgPriceTable extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            prices: [
                { instrument: 'Galactica', type: 'Buy', avgPrice: '123.586245' },
                { instrument: 'Galactia', type: 'Sell', avgPrice: '195.5968' },
                { instrument: 'Deuteronic', type: 'Buy', avgPrice: '325.945' },
                { instrument: 'Deuteronic', type: 'Sell', avgPrice: '245.945665' },
                { instrument: 'Lunatic', type: 'Buy', avgPrice: '325.945' },
                { instrument: 'Lunatic', type: 'Sell', avgPrice: '245.945665' },
                { instrument: 'Astronomica', type: 'Buy', avgPrice: '325.945' },
                { instrument: 'Astronomica', type: 'Sell', avgPrice: '245.945665' },
                { instrument: 'Heliosphere', type: 'Buy', avgPrice: '325.945' },
                { instrument: 'Heliosphere', type: 'Sell', avgPrice: '245.945665' },
            ]
        }
    }

    renderTableHeader() {
        let header = Object.keys(this.state.prices[0])
        return header.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    renderTableData() {
        return this.state.prices.map((price, index) => {
            const { instrument, type, avgPrice} = price //destructuring
            return (
                <tr key={instrument}>
                    <td>{instrument}</td>
                    <td>{type}</td>
                    <td>{avgPrice}</td>
                </tr>
            )
        })
    }

    render() {
        return (
            
            <div class="datatable">
                <table id='avprices'>
                    <tbody>
                        <tr>{this.renderTableHeader()}</tr>
                        {this.renderTableData()}
                    </tbody>
                </table>
            </div>

        )
    }

}

//export default GraphApp;
export default ShowGraph;
