import React, { useState } from "react";
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './css/style.css';
import { Line } from 'react-chartjs-2';
import trimHistory from './trim_history.js';
import ShowGraph from './graphpage.js';
import HistoryMain from './historypage.js';
import Table from './positionpage.js';


const API_url = 'http://localhost:6001'

function TradeApp() {

    return (
        <React.Fragment>

            <div className="sidenav">
                <h3><b>Crocagile</b></h3>
                <a href="/graph">Live Data</a>
                <a href="/history">History</a>
                <a href="/position">Position</a>
            </div>

            <nav className="navbar navbar-dark bg-dark">
                <a className="navbar-brand" href="#">Navbar</a>
            </nav>

            <ShowGraph />
            <HistoryMain />
            <Table />

        </React.Fragment>
    );
}


export default TradeApp;
