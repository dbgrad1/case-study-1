import React, { useState } from "react";
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './css/style.css';

const API_url = 'http://localhost:6002'

function PositionApp() {

    return (
    <React.Fragment>

      <div className="sidenav">
            <h3><b>Crocagile</b></h3>
            <a href="/graph">Live Data</a>
            <a href="/history">History</a>
            <a href="/position">Position</a>
      </div>
  
      <nav className="navbar navbar-dark bg-dark">
        <a className="navbar-brand" href="#">Navbar</a>
      </nav>

      <div className="heading">  
        <h1 align="center"><b>Financial Position</b></h1>
      </div>


      <div className="footer">
        <p>© 2019 Copyright: Crocagile Ltd.</p>
      </div>

      <Table />
    </React.Fragment>
    );
}

/* building a dynamic table*/

class Table extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            profits: [
                { dealer: 'Selvyn', realizedProfit: '9833.53', effectiveProfit: '7845.65' },
                { dealer: 'Travis', realizedProfit: '-2.654', effectiveProfit: '-3.5652' },
                { dealer: 'Victoria', realizedProfit: '5064.456852', effectiveProfit: '4686.66666' },
                { dealer: 'Ed', realizedProfit: '7000.7777', effectiveProfit: '6999.99999999999' }
            ]
        }
    }

    renderTableHeader() {
        let header = Object.keys(this.state.profits[0])
        return header.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    renderTableData() {
        return this.state.profits.map((profit, index) => {
            const { dealer, realizedProfit, effectiveProfit} = profit //destructuring
            return (
                <tr key={dealer}>
                    <td>{dealer}</td>
                    <td>{realizedProfit}</td>
                    <td>{effectiveProfit}</td>
                </tr>
            )
        })
    }

    render() {
        return (
            
            <div class="datatable">
                <table id='instruments'>
                    <tbody>
                        <tr>{this.renderTableHeader()}</tr>
                        {this.renderTableData()}
                    </tbody>
                </table>
            </div>

        )
    }

}


export default Table;
//export default PositionApp;
