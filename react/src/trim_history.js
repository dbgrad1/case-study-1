function trimHistory(deals,n){

    var instruments = new Map();

    
    for(var d = 0; d < deals.length; d++) {
        var i = deals[d]['instrument'];
    
        if (instruments.has(i) === true){
            var price_vals = instruments.get(i);
            price_vals.push(deals[d]['price']);
    
            if (price_vals.length > n){
                var latest_vals = price_vals.slice(price_vals.length-n,price_vals.length);
                instruments.set(i,latest_vals);
                document.write(instruments.get(i)+'\n');
            } 
        }
    
        else {instruments.set(i,[]);}
     }   

     return instruments
    
}

export default trimHistory;
    