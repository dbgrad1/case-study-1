import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './css/style.css';
import mainLogo from './images/mainLogo.png'

const API_url='http://localhost:5000'

function LoginView() {
    return (
      <div className='wrapper flex-center flex-column'>
        <img src={mainLogo} height="200px" width="200px" alt="mainLogo"/>
        <h3><b>Crocagile</b></h3>
        <h5 className="animated fadeIn mb-3">The Agile Tool for Traders</h5>
        <LoginForm />
      </div>
    );
}

function LoginForm() {
const [email, setEmail] = useState("");
const [pwd, setPwd] = useState("");
const [, updateState] = React.useState();
const forceUpdate = React.useCallback(() => updateState({}), []);

const handleSubmit = (evt) => {
  evt.preventDefault();
  const request = new Request(API_url+'/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({email: email, password: pwd})
  })
  fetch(request)
    .then(res => {
      if (res.status === 200) {
        res.json()
        .then(res => {
          localStorage.setItem('access_token', res.access_token);
          forceUpdate();
        });
      }
    })
}

if ((localStorage.getItem('access_token') != null))
    return <Redirect to='/secretStaff'/>;
else 
  return (
    <form>
      <div className="form-group">
        <label>Email:</label>
        <input type="email" className="form-control" id="email" value={email} onChange={e => setEmail(e.target.value)} />
      </div>
      <div className="form-group">
        <label>Password:</label>
        <input type="password" className="form-control" id="pwd" value={pwd} onChange={e => setPwd(e.target.value)} />
      </div>
      <button type="submit" className="btn btn-default" onClick = {handleSubmit}> Get Started </button>
    </form>
  );
}

export default LoginView