//random colour generator
function colorGen(){
    var r = Math.random() * 256;
    var g = Math.random() * 256;
    var b = Math.random() * 256;
    return [r,g,b];
}


//create line for each instrument
function lineMaker(name,prices){

    var col = colorGen(); //generate random line colour

    var colstring = 'rgba('+col[0]+','+col[1]+','+col[2]+',';
    var bgcol = colstring+'0.5)';
    var bdcol = colstring+'1)';

    var idata = {
        label: name,
        fill: false,
        lineTension: 0.1,
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,65,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: prices,
        backgroundColor: bgcol,
        borderColor: bdcol,
        pointBorderColor: bdcol
    };
    
    return idata;

}

function graphMaker(instrumentMap,n){
    var showData = []; //all the data that goes on to the graph - equiv to data
    showData['datasets'] = [];

    var x_vals = [] //stores time points for x-axis
    for (var i = n; i >= 1; i--) {
        x_vals.push(i);
     }

    showData['labels'] = x_vals;

    //create new data element for each instrument - colours and all
    for (const [iname, iprice] of instrumentMap.entries()){
        var idata = lineMaker(iname,iprice);
        showData['datasets'].push(idata);
    }
    
    return showData;
}

export default graphMaker;