import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './css/style.css';
import LoginView from './login.js'
import HistoryView from './historypage.js';
import GraphApp from './graphpage.js';
import PositionApp from './positionpage.js';
import TraderApp from './traderapp.js';


function App() {
  return (
    <Router>
      <Switch>
        <Route
          path="/login"
          exact
          render={() =>  <LoginView />}
        />
        <Route 
          path="/history"
          exact
          render={() => <HistoryView />}
        />
        <Route 
          path="/graph"
          exact
          render={() => <GraphApp />}
        />
        <Route 
          path="/position"
          exact
          render={() => <PositionApp />}
        />
        <Route 
          path="/trade"
          exact
          render={() => <TraderApp />}
        />
      </Switch>
    </Router>
  );
}

export default App;
