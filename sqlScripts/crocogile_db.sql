SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema crocogile
-- -----------------------------------------------------

CREATE SCHEMA IF NOT EXISTS `crocogile` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `crocogile` ;

-- -----------------------------------------------------
-- Table `crocogile`.`Instrument`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crocogile`.`Instrument`;
CREATE TABLE IF NOT EXISTS `crocogile`.`Instrument` (
  `idInstrument` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idInstrument`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `crocogile`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crocogile`.`User`;
CREATE TABLE IF NOT EXISTS `crocogile`.`User` (
  `idUser` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `hashedPassword` VARCHAR(45) NOT NULL,
  `role` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`idUser`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `crocogile`.`Dealer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crocogile`.`Dealer`;
CREATE TABLE IF NOT EXISTS `crocogile`.`Dealer` (
  `idDealer` INT(11) NOT NULL AUTO_INCREMENT,
  `idUser` INT(11),
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idDealer`),
  FOREIGN KEY (`idUser`) REFERENCES User(`idUser`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;



-- -----------------------------------------------------
-- Table `crocogile`.`Deal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `crocogile`.`Deal`;
CREATE TABLE IF NOT EXISTS `crocogile`.`Deal` (
  `idDeal` INT(11) NOT NULL AUTO_INCREMENT,
  `price` FLOAT NOT NULL,
  `type` CHAR(1) NOT NULL,
  `quantity` INT(11) NOT NULL,
  `time` DATETIME NOT NULL,
  `idInstrument` INT(11) NOT NULL,
  `idDealer` INT(11) NOT NULL,
  PRIMARY KEY (`idDeal`),
  INDEX `idInstrument` (`idInstrument` ASC) VISIBLE,
  INDEX `Deal_ibfk_2` (`idDealer` ASC) VISIBLE,
  CONSTRAINT `Deal_ibfk_1`
    FOREIGN KEY (`idInstrument`)
    REFERENCES `crocogile`.`Instrument` (`idInstrument`),
  CONSTRAINT `Deal_ibfk_2`
    FOREIGN KEY (`idDealer`)
    REFERENCES `crocogile`.`Dealer` (`idDealer`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
