**Deutsche Bank Graduate Programme 2019 - Case Study Group 1**

Access Case Study Requirements and Specifications from:

URL: https://deliveringtechnology2018.atlassian.net/wiki/spaces/CS2D/pages/790167560/Case+Study+-+Technical+Details 

Login: db.grad@celestial.co.uk

Password: db.grad@2019prog

---

# Git mannual
Before you start develop something new  to BitBucket repo y'd better to make sure that you are on dev branch on your machine and that your LOCAL dev branch is up to date with repo's dev branch. To do that type 
```
git checkout dev
git pull origin dev
```

Now you have to fork from dev branch to local branch and commit all changes into that new local branch. Let's call it my_cool_new_feature (just example).
```
git checkout -b my_cool_new_feature
```

Now it is time for coding and after have your coding finished commit changes localy in your branch my_cool_new_feature.

```
git commit -m 'Added my_cool_new_feature'
```

Now checkout to dev branch and merge changes from my_cool_new_feature branch to dev branch

```
git checkout dev
git merge my_cool_new_feature
```

At this moment if you have any warnings about MERGRE CONFLICTs and you are not familliar with it just ask somebody who knows this staff :)
Otherwise if everything is cool with merging just push it to origin (BitBucket)

```
git push origin dev
```

And don't forget to fork again to one_more_cool_new_feature branch when start develop again.


# Run a Docker Container

docker-machine start

docker build . -t <image name>

docker run -p 8080:80 <image name>

# Get list of all containers
docker ps -a 

# Get list of running containers
docker ps

# Kill a container
docker ps

docker kill <first few letters of the container id>





---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).