from flask import Flask, jsonify
import mysql.connector
from mysql.connector import Error
from flask import request
from flask import abort
import json
from datetime import datetime

app = Flask(__name__)
host = 'mysql'
database = 'crocagile_db'
user = 'root'
password = 'ppp'


@app.route("/")
def base():
    return "DAO is working"


@app.route('/getDealData', methods=['GET'])
def getDealData():
    try:
        cnx = mysql.connector.connect(host=host, database=database, user=user, password=password)
        cursor = cnx.cursor()
        cursor.execute('''SELECT d.idDeal,d.price,d.type,d.quantity,d.time,i.name,dr.name FROM Deal d
                                JOIN Dealer dr on dr.idDealer = d.idDealer 
                                JOIN Instrument i on i.idInstrument = d.idInstrument''')
        result = cursor.fetchall()
        data = []
        if len(result) == 0:
            abort(404)
        for row in result:
            data.append({'idDeal': row[0],
                         'price': row[1],
                         'type': row[2],
                         'quantity': row[3],
                         'time': row[4],
                         'instrument': row[5],
                         'dealer': row[6]
                         })
        cursor.close()
        cnx.close()
        print(data)

        return jsonify(data), 200

    except Error as e:
        return jsonify({'error': e.msg}), 401


@app.route("/getUser", methods=['GET'])
def getUser():

    try:
        cnx = mysql.connector.connect(host=host, database=database, user=user, password=password)
        cursor = cnx.cursor()
        req = request.get_json(silent=True)
        email = req.get('email', None)

        cursor.execute('''SELECT email, hashedPassword, role
                        FROM User u
                        WHERE u.email = {}'''.format(email))
        rv = cursor.fetchall()

        cursor.close()
        cnx.close()

        if len(rv) == 0:
            abort(404)
        else:
            result = rv[0]
            return jsonify(email=result[0], hashedPassword=result[1], role=result[2]), 200

    except Error as e:
        return jsonify({'error': e.msg}), 401


@app.route("/addUser", methods=['POST'])
def addUser():
    try:
        cnx = mysql.connector.connect(host=host, database=database, user=user, password=password)
        cursor = cnx.cursor()
        req = request.get_json(silent=True)
        jsonData = json.loads(req)
        email = jsonData['email']
        hashedPassword = jsonData['hashedPassword']
        role = jsonData['role']

        cursor.execute('''SELECT * FROM User WHERE email = {}'''.format(email))
        row = cursor.fetchone()
        if row is not None:
            abort(400)
        else:
            cursor.execute('''INSERT INTO User (email, hashedPassword, role) VALUES ({},{},{})'''.format(email, hashedPassword, role))
            idDealer = cursor.lastrowid
            cnx.commit()

        cursor.close()
        cnx.close()
        return jsonify({}), 201

    except Error as e:
        return jsonify({'error': e.msg}), 401


@app.route("/insert", methods=['POST'])
def insert():
    try:
        cnx = mysql.connector.connect(host=host, database=database, user=user, password=password)
        cursor = cnx.cursor()
        if len(request.data) <= 2: abort(403)
        req = request.get_json()
        jsonData = json.loads(req)
        instrumentName = jsonData['instrumentName']
        dealerName = jsonData['cpty']
        price = jsonData['price']
        type = jsonData['type']
        quantity = jsonData['quantity']
        time = jsonData['time']
        time = datetime.strptime(time, '%d-%b-%Y (%H:%M:%S.%f)').strftime("%Y-%m-%d %H:%M:%S.%f")

        # getting dealer or create
        cursor.execute('''SELECT idDealer FROM Dealer WHERE name = "{}"'''.format(dealerName))
        row = cursor.fetchone()
        if row is not None:
            idDealer = row[0]
        else:
            cursor.execute('''INSERT INTO Dealer (name) VALUES ("{}")'''.format(dealerName))
            idDealer = cursor.lastrowid
            cnx.commit()

        # getting instrument or create
        cursor.execute('''SELECT idInstrument FROM Instrument WHERE name = "{}"'''.format(instrumentName))
        row = cursor.fetchone()
        if row is not None:
            idInstrument = row[0]
        else:
            cursor.execute('''INSERT INTO Instrument (name) VALUES ("{}")'''.format(instrumentName))
            idInstrument = cursor.lastrowid
            cnx.commit()

        # creating a deal
        cursor.execute('''INSERT INTO Deal (idInstrument,idDealer,price,type,quantity,time)
                        VALUES ({},{},{},"{}",{},"{}")'''.format(idInstrument, idDealer, price, type, quantity, time))
        cnx.commit()

        cursor.close()
        cnx.close()
        print('OK')
        return jsonify({}), 201

    except Error as e:
        print(e.msg)
        return jsonify({'error': e.msg}), 401

def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))

if __name__ == '__main__':
    app.run()

