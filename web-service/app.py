from middlewares import authorize
from flask import Flask, request, jsonify
import jwt
import json
import requests
import time
from flask_cors import CORS

CORE_IP = 'http://core1-crocagile.apps.dbgrads-6eec.openshiftworkshop.com'

with open('config.json', 'r') as f:
    config = json.load(f)

class User:
    def __init__(self, email, password):
        self.email = email
        self.password = password

app = Flask(__name__)
CORS(app)

userList = [User("foo@gmail.com", "bar")]

@app.route('/addUser', methods=['POST'])
def addUser():
    req = request.get_json(silent=True)
    email = req.get('email', None)
    password = req.get('password', None)
    userList.append(User(email, password))
    print("New user added ")
    print("Email:%s"%(email))
    print('Password:%s\n'%(password))
    ret={}
    return (jsonify(ret), 200)


@app.route('/login', methods=['POST'])
def login():
    """
    Logs a user in by parsing a POST request containing user credentials and
    issuing a JWT token.
    .. example::
       $ curl http://localhost:5000/login -X POST \
         -d '{"email":"Walter@gmail.com","password":"calmerthanyouare"}'
    """
    req = request.get_json(silent=True)
    email = req.get('email', None)
    password = req.get('password', None)
    print('Email:%s'%(email))
    print('Password:%s\n'%(password))
    index = [i for i in range(len(userList)) if userList[i].email == email]
    if (index == []):
        return (jsonify({}), 403)
    else:
        index = index[0]
        if (userList[index].password != password):
            return (jsonify({}), 403)
        else:
            token = jwt.encode({'email': email}, config['jwt_secret'], algorithm='HS256')
            ret={'access_token': token.decode()}
            return (jsonify(ret), 200)

@app.route('/secretStaff', methods=['GET'])
@authorize
def get_secret_stuff(email):
    return (jsonify({'email':email, 'secretStaff':'Crogagile is the best!'}), 200)

@app.route('/getHistory', methods=['POST'])
def get_history():
    req = request.get_json(silent=True)
    instrument = req.get('instrument', None)
    _from = req.get('from', None)
    to = req.get('to', None)
    name = req.get('name', None)
    print(instrument, _from, to, name)
    body = {"from": _from,
            "to": to}
    if (instrument is not None):
        body['instrument']=instrument
    if (name is not None):
        body['name']=name
    r = requests.get(CORE_IP+'/getHistory', 
                     json=body)
    return r.content

# eventq = Queue()

# @app.route('/liveUpdate', methods = ['POST'])
# def post_live_update():
#     req = request.get_json(silent=True)
#     jsonData = json.loads(req)
#     q.put(jsonData)


# @route("/streamAll")
# def stream():
#     def event_stream():
#         while True:
#             if (not q.empty()):
#                 yield q.get()
#     return Response(event_stream(), mimetype="text/event-stream")


def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))

if __name__ == '__main__':
    bootapp()