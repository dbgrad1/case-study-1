from functools import wraps
from flask import abort, request
import jwt
import json 

with open('config.json', 'r') as f:
    config = json.load(f)

def authorize(f):
    @wraps(f)
    def decorated_function(*args, **kws):
            if not 'Authorization' in request.headers:
               abort(401)

            email = None
            data = request.headers['Authorization']
            token = str.replace(str(data), 'Bearer ','')
            try:
                email = jwt.decode(token, config['jwt_secret'], algorithms=['HS256'])['email']
            except:
                abort(401)

            return f(email, *args, **kws)            
    return decorated_function