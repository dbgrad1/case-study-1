from flask import Flask, Response, request
from flask_cors import CORS
import RandomDealData

app = Flask(__name__)
CORS(app)

@app.route('/stream')
def stream():
    def eventStream():
        while True:
            yield '{}\n\n'.format(RandomDealData.stream())
    return Response(eventStream(), mimetype="text/event-stream")


@app.route('/ReceiveStream', methods = ['POST'])
def receive():
    return 'ok'


        
def bootapp():
    app.run(port=8080, threaded=True, host=('0.0.0.0'))

if __name__ == '__main__':
    bootapp()